<?php
include('./webservices/connection/connection.php');
session_start();
$id = $_SESSION['id'];
$package = $db->query("SELECT * FROM `corier_package` WHERE user_id='$id'");

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Admin, Dashboard, Bootstrap" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>DASHBOARD</title>
    <link rel="stylesheet" href="./fonts/md-fonts/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="./fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="./libs/animate.css/animate.min.css">
    <link rel="stylesheet" href="./libs/jquery-loading/dist/jquery.loading.min.css">

    <link id="pageStyle" rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="./libs/tables-datatables/dist/datatables.min.css">
</head>

<body class="app sidebar-fixed aside-menu-off-canvas aside-menu-hidden header-fixed ">
    <?php include('header.php') ?>

    <div class="app-body">
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-accent-theme">
                            <div class="card-body">
                                <h4 class="text-theme">Your Package</h4>
                                <br />

                                <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable" width="100%">
                                    <thead>
                                        <tr>
                                            <th>courier name</th>
                                            <th>courier email</th>
                                            <th>Track id</th>
                                            <th>title</th>
                                            <th>description</th>
                                            <th>weight</th>
                                            <th>status</th>
                                            <th>location</th>
                                            <th>address</th>
                                            <th> delivery_date</th>

                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php while ($fdata = $package->fetch(PDO::FETCH_ASSOC)) : ?>
                                            <?php

                                            $recieverID = $fdata['to_id'];
                                            $reciever = $db->query("SELECT * FROM `user` WHERE id='$recieverID'");
                                            $freciever = $reciever->fetch(PDO::FETCH_ASSOC);
                                            ?>
                                            <tr>

                                                <td> <?php echo "" . $freciever['name'] . ""; ?>
                                            phone:<?php echo "" . $freciever['phone'] . ""; ?> 
                                            </td>
                                                <td> <?php echo "" . $freciever['email'] . ""; ?></td>
                                                <td> <?php echo "" . $fdata['hash_id'] . ""; ?></td>
                                                <td> <?php echo "" . $fdata['title'] . ""; ?></td>

                                                <td> <?php echo "" . $fdata['description'] . ""; ?></td>
                                                <td>
                                                    <?php echo "" . $fdata['weight'] . ""; ?>
                                                </td>


                                                <td>

                                                    <?php
                                                    if ($fdata['status'] == "declined") {
                                                    ?>
                                                        <span class="badge badge-boxed badge-danger"> <?php echo "" . $fdata['status'] . ""; ?></span>
                                                    <?php
                                                    } else {
                                                    ?>
                                                        <span class="badge badge-boxed badge-info"> <?php echo "" . $fdata['status'] . ""; ?></span>
                                                    <?php
                                                    }
                                                    ?>

                                                </td>
                                                <td>
                                                    <?php echo "" . $fdata['location'] . ""; ?>
                                                </td>
                                                <td>
                                                    <?php echo "" . $fdata['address'] . ""; ?>
                                                </td>
                                                <td>
                                                    <?php echo "" . $fdata['delivery_date'] . ""; ?>
                                                </td>

                                            </tr>
                                        <?php endwhile; ?>
                                    </tbody>

                                </table>

                            </div>
                            <!-- end card-body -->
                            <div id="user_details"></div>
                            <div id="user_model_details"></div>
                        </div>
                        <!-- end card -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="./libs/jquery/dist/jquery.min.js"></script>
    <script src="./libs/nicescroll/jquery.nicescroll.min.js"></script>
    <script src="./libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="./libs/bootstrap/bootstrap.min.js"></script>
    <script src="./libs/PACE/pace.min.js"></script>
    <script src="./libs/chart.js/dist/Chart.min.js"></script>


    <!-- jquery-loading -->
    <script src="./libs/jquery-loading/dist/jquery.loading.min.js"></script>
    <!--datatables -->
    <script src="./libs/tables-datatables/dist/datatables.min.js"></script>



    <!-- octadmin Main Script -->
    <script src="./js/app.js"></script>

    <!-- datatable examples -->
    <script src="./js/table-datatable-example.js"></script>


</body>

</html>