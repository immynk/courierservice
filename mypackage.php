<?php
include('./webservices/connection/connection.php');
session_start();
$id = $_SESSION['id'];
$package = $db->query("SELECT * FROM `package` WHERE user_id='$id'");

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Admin, Dashboard, Bootstrap" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>DASHBOARD</title>
    <link rel="stylesheet" href="./fonts/md-fonts/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="./fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="./libs/animate.css/animate.min.css">
    <link rel="stylesheet" href="./libs/jquery-loading/dist/jquery.loading.min.css">

    <link id="pageStyle" rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="./libs/tables-datatables/dist/datatables.min.css">
</head>

<body class="app sidebar-fixed aside-menu-off-canvas aside-menu-hidden header-fixed ">
    <?php include('header.php') ?>

    <div class="app-body">
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-accent-theme">
                            <div class="card-body">
                                <h4 class="text-theme">Your Package</h4>
                                <br />

                                <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable" width="100%">
                                    <thead>
                                        <tr>
                                            <th>From</th>
                                            <th>To</th>
                                            <th>title</th>
                                            <th>weight</th>
                                            <th>Delivery Address</th>
                                            <th>Delivery Date</th>
                                            <th>Delivery status</th>
                                            <th> Assigned Name</th>
                                            <th>Track Id</th>
                                            <th>Payment status</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php while ($fdata = $package->fetch(PDO::FETCH_ASSOC)) : ?>
                                            <?php
                                            $senderID = $fdata['user_id'];
                                            $recieverID = $fdata['to_id'];
                                            $assignID = $fdata['assign_id'];
                                            $paymentID = $fdata['hash_id'];

                                            $sender = $db->query("SELECT * FROM `user` WHERE id='$senderID'");
                                            $fsender = $sender->fetch(PDO::FETCH_ASSOC);

                                            $reciever = $db->query("SELECT * FROM `user` WHERE id='$recieverID'");
                                            $freciever = $reciever->fetch(PDO::FETCH_ASSOC);

                                            $assigned = $db->query("SELECT * FROM `user` WHERE id='$assignID'");
                                            $fassigned = $assigned->fetch(PDO::FETCH_ASSOC);

                                            $payment = $db->query("SELECT * FROM `payment` WHERE package_id='$paymentID'");
                                            $fpayment = $payment->fetch(PDO::FETCH_ASSOC);
                                            ?>
                                            <tr>
                                                <td> <?php echo "" . $fsender['name'] . ""; ?></td>
                                                <td> <?php echo "" . $freciever['name'] . ""; ?></td>
                                                <td> <?php echo "" . $fdata['title'] . ""; ?></td>
                                                <td> <?php echo "" . $fdata['weight'] . ""; ?></td>

                                                <td> <?php echo "" . $fdata['address'] . ""; ?></td>
                                                <td>
                                                    <?php echo "" . $fdata['delivery_date'] . ""; ?>
                                                </td>
                                                <td>

                                                    <?php
                                                    if ($fdata['status'] == "declined") {
                                                    ?>
                                                        <span class="badge badge-boxed badge-danger"> <?php echo "" . $fdata['status'] . ""; ?></span>
                                                    <?php
                                                    } else {
                                                    ?>
                                                        <span class="badge badge-boxed badge-info"> <?php echo "" . $fdata['status'] . ""; ?></span>
                                                    <?php
                                                    }
                                                    ?>

                                                </td>
                                                <!-- <td>
                                                    <?php echo "" . $fassigned['name'] . ""; ?> -->
                                                <td><button type="button" class=" badge badge-boxed badge-success btn-xs start_chat" data-fromuserid="<?= $senderID ?>" data-touserid="<?php echo "" . $fassigned['id'] . ""; ?>" data-tousername="<?php echo "" . $fassigned['name'] . ""; ?>"> <?php echo "" . $fassigned['name'] . ""; ?></button></td>
                                                <!-- </td> -->
                                                <td>
                                                    <?php echo "" . $fdata['hash_id'] . ""; ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if ($fpayment['status'] == "pending") {
                                                    ?>
                                                        <span class=" badge badge-boxed badge-danger"> <?php echo "" . $fpayment['status'] . ""; ?></span>
                                                    <?php
                                                    } else {
                                                    ?>
                                                        <span class=" badge badge-boxed badge-success"> <?php echo "" . $fpayment['status'] . ""; ?></span>
                                                    <?php
                                                    }
                                                    ?>


                                                </td>
                                            </tr>
                                        <?php endwhile; ?>
                                    </tbody>

                                </table>

                            </div>
                            <!-- end card-body -->
                            <div id="user_details"></div>
                            <div id="user_model_details"></div>
                        </div>
                        <!-- end card -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="./libs/jquery/dist/jquery.min.js"></script>
    <script src="./libs/nicescroll/jquery.nicescroll.min.js"></script>
    <script src="./libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="./libs/bootstrap/bootstrap.min.js"></script>
    <script src="./libs/PACE/pace.min.js"></script>
    <script src="./libs/chart.js/dist/Chart.min.js"></script>


    <!-- jquery-loading -->
    <script src="./libs/jquery-loading/dist/jquery.loading.min.js"></script>
    <!--datatables -->
    <script src="./libs/tables-datatables/dist/datatables.min.js"></script>



    <!-- octadmin Main Script -->
    <script src="./js/app.js"></script>

    <!-- datatable examples -->
    <script src="./js/table-datatable-example.js"></script>
    <script>
        $(document).ready(function() {

            function make_chat_dialog_box(to_user_id, to_user_name) {
                var modal_content = '<div id="user_dialog_' + to_user_id + '" class="user_dialog" title="You have chat with ' + to_user_name + '">';
                modal_content += '<div style="height:400px; border:1px solid #ccc; overflow-y: scroll; margin-bottom:24px; padding:16px;" class="chat_history" data-touserid="' + to_user_id + '" id="chat_history_' + to_user_id + '">';
                modal_content += fetch_user_chat_history(to_user_id);
                modal_content += '</div>';
                modal_content += '<div class="form-group">';
                modal_content += '<textarea name="chat_message_' + to_user_id + '" id="chat_message_' + to_user_id + '" class="form-control"></textarea>';
                modal_content += '</div><div class="form-group" align="right">';
                modal_content += '<button type="button" name="send_chat" id="' + to_user_id + '" class="btn btn-info send_chat">Send</button></div></div>';
                $('#user_model_details').html(modal_content);
            }

            $(document).on('click', '.start_chat', function() {
                var to_user_id = $(this).data('touserid');
                var to_user_name = $(this).data('tousername');
                
                make_chat_dialog_box(to_user_id, to_user_name);
                $("#user_dialog_" + to_user_id).dialog({
                    autoOpen: false,
                    width: 400
                });
                $('#user_dialog_' + to_user_id).dialog('open');
            });

            $(document).on('click', '.send_chat', function() {
                var to_user_id = $(this).attr('id');
                var chat_message = $('#chat_message_' + to_user_id).val();
                $.ajax({
                    url: "./webservices/ajax_chat.php",
                    method: "POST",
                    data: {

                        to_user_id: to_user_id,
                        chat_message: chat_message
                    },
                    success: function(data) {
                        $('#chat_message_' + to_user_id).val('');
                        $('#chat_history_' + to_user_id).html(data);
                    }
                })
            });

            function fetch_user_chat_history(to_user_id) {
                $.ajax({
                    url: "./webservices/ajax_chathistory.php",
                    method: "POST",
                    data: {
                        to_user_id: to_user_id
                    },
                    success: function(data) {
                        $('#chat_history_' + to_user_id).html(data);
                    }
                })
            }

            function update_chat_history_data() {
                $('.chat_history').each(function() {
                    var to_user_id = $(this).data('touserid');
                    fetch_user_chat_history(to_user_id);
                });
            }
        });
    </script>


</body>

</html>