<?php
include('./webservices/connection/connection.php');
session_start();
$id = $_SESSION['id'];
$to_id = $_GET['id'];
$mypackage = $db->query("SELECT * FROM `package` WHERE assign_id='$id'");

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Admin, Dashboard, Bootstrap" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Assign package</title>
    <link rel="stylesheet" href="./fonts/md-fonts/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="./fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="./libs/animate.css/animate.min.css">
    <link rel="stylesheet" href="./libs/jquery-loading/dist/jquery.loading.min.css">

    <link id="pageStyle" rel="stylesheet" href="./css/style.css">

</head>

<body class="app sidebar-fixed aside-menu-off-canvas aside-menu-hidden header-fixed ">
    <?php include('header.php') ?>

    <div class="app-body">
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-accent-theme">
                            <div class="card-body">
                                <form id="assign_package" method="post">
                                    <div class="card">
                                        <div class="card-header text-theme">
                                            <strong>assign package to courier</strong>
                                            <small></small>
                                        </div>

                                        <input type="hidden" value="<?= $to_id ?>" name="to_id">
                                        <input type="hidden" value="<?= $id ?>" name="user_id">

                                        <div class="card-body">
                                            <select required name="assign" class="form-control">
                                                <?php while ($fdata = $mypackage->fetch(PDO::FETCH_ASSOC)) : ?>

                                                    <option value="<?= $fdata['hash_id'] ?>"><?= $fdata['hash_id'] ?></option>


                                                <?php endwhile; ?>
                                            </select>

                                            <div class="alert alert-success" id="success" role="alert" style="display: none;"></div>
                                            <div class="alert alert-danger" id="warning" role="alert" style="display: none;"></div>
                                            <div class="row">
                                                <button type="submit" class="btn btn-theme btn-sm"><i class="fa fa-dot-circle-o"></i>assign courier</button>
                                            </div>
                                            <!--/.row-->
                                        </div>
                                        <!-- end card-body -->
                                    </div>
                                </form>


                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="./libs/jquery/dist/jquery.min.js"></script>
    <script src="./libs/nicescroll/jquery.nicescroll.min.js"></script>
    <script src="./libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="./libs/bootstrap/bootstrap.min.js"></script>
    <script src="./libs/PACE/pace.min.js"></script>
    <script src="./libs/chart.js/dist/Chart.min.js"></script>


    <!-- jquery-loading -->
    <script src="./libs/jquery-loading/dist/jquery.loading.min.js"></script>



    <!-- octadmin Main Script -->
    <script src="./js/app.js"></script>

    <!-- datatable examples -->
    <script src="./js/table-datatable-example.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#assign_package").on('submit', (function(e) {
                e.preventDefault();
                $.ajax({
                    url: "./webservices/ajax_assigncourier.php",
                    type: "POST",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data) {
                        var obj = $.parseJSON(data);
                        if (obj.success == "success") {
                            $("#success").show();
                            $("#success").html("Package assigned successfully");
                            $("#success").fadeOut(4000);
                            window.setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }
                        if (obj.success == "fail") {
                            $("#warning").show();
                            $("#warning").html("assign package After Sometime");
                            $("#warning").fadeOut(4000);
                            window.setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }

                    }
                });
            }));
        });
    </script>


</body>

</html>