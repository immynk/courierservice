<?php
include('./webservices/connection/connection.php');
session_start();


?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Admin, Dashboard, Bootstrap" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>DASHBOARD</title>
    <link rel="stylesheet" href="../fonts/md-fonts/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../libs/animate.css/animate.min.css">
    <link rel="stylesheet" href="../libs/jquery-loading/dist/jquery.loading.min.css">

    <link id="pageStyle" rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../libs/tables-datatables/dist/datatables.min.css">
</head>

<body class="app sidebar-fixed aside-menu-off-canvas aside-menu-hidden header-fixed ">
    <?php include('header.php') ?>
    <div class="app-body">
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-accent-theme">
                            <div class="card-body">
                                <div class="row">

                                    <!-- Add new Admin -->

                                    <div class="col-sm-12">
                                        <div class="card">
                                            <div class="card-header text-theme">
                                                <strong>Create branch</strong>
                                                <small></small>
                                            </div>
                                            <div class="card-body">
                                                <form id="add_branch" method="post">

                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="name">title</label>
                                                                <input type="text" required name="title" class="form-control" id="title" placeholder="Enter title">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="name"> city</label>
                                                                <input type="text" required name="city" class="form-control" id="city" placeholder="Enter city">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="name"> address</label>
                                                                <input type="text" required name="address" class="form-control" id="address" placeholder="Enter  address">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="name">zipcode</label>
                                                                <input type="text" required name="zipcode" class="form-control" id="zipcode" placeholder="Enter  zipcode">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="alert alert-success" id="success" role="alert" style="display: none;"></div>
                                                    <div class="alert alert-danger" id="warning" role="alert" style="display: none;"></div>
                                                    <button type="submit" class="btn btn-sm btn-primary">
                                                        <i class="fa fa-dot-circle-o"></i> Add branch</button>
                                                </form>
                                            </div>
                                        </div>

                                    </div>


                                </div>



                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../libs/jquery/dist/jquery.min.js"></script>
    <script src="../libs/nicescroll/jquery.nicescroll.min.js"></script>
    <script src="../libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../libs/bootstrap/bootstrap.min.js"></script>
    <script src="../libs/PACE/pace.min.js"></script>
    <script src="../libs/chart.js/dist/Chart.min.js"></script>
    <script src="../libs/jquery-loading/dist/jquery.loading.min.js"></script>
    <script src="../libs/tables-datatables/dist/datatables.min.js"></script>
    <script src="../js/app.js"></script>
    <script src="../js/table-datatable-example.js"></script>




    <script type="text/javascript">
        $(document).ready(function() {
            $("#add_branch").on('submit', (function(e) {
                e.preventDefault();
                $.ajax({
                    url: "./webservices/ajax_create_branch.php",
                    type: "POST",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data) {
                        var obj = $.parseJSON(data);
                        if (obj.success == "success") {
                            $("#success").show();
                            $("#success").html("Branch added sucessfully");
                            $("#success").fadeOut(4000);
                            window.setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }



                        if (obj.success == "fail") {
                            $("#warning").show();
                            $("#warning").html("try updating After Sometime");
                            $("#warning").fadeOut(4000);
                            window.setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }

                    }
                });
            }));
        });
    </script>

</body>

</html>