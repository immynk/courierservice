<?php
include('../webservices/connection/connection.php');
session_start();

$emp = $db->query("SELECT * FROM `user` WHERE type=2");

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Admin, Dashboard, Bootstrap" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>DASHBOARD</title>
    <link rel="stylesheet" href="../fonts/md-fonts/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../libs/animate.css/animate.min.css">
    <link rel="stylesheet" href="../libs/jquery-loading/dist/jquery.loading.min.css">

    <link id="pageStyle" rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../libs/tables-datatables/dist/datatables.min.css">
</head>

<body class="app sidebar-fixed aside-menu-off-canvas aside-menu-hidden header-fixed ">
    <?php include('header.php') ?>

    <div class="app-body">
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-accent-theme">
                            <div class="card-body">
                                <h4 class="text-theme">employee list</h4>
                                <br />

                                <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable" width="100%">
                                    <thead>
                                        <tr>
                                            <th>name</th>
                                            <th>email</th>
                                            <th>phone</th>
                                            <th>address</th>
                                            <th>city</th>
                                            <th>zipcode</th>

                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php while ($fdata = $emp->fetch(PDO::FETCH_ASSOC)) : ?>

                                            <tr>
                                                <td> <?php echo "" . $fdata['name'] . ""; ?></td>
                                                <td> <?php echo "" . $fdata['email'] . ""; ?></td>
                                                <td> <?php echo "" . $fdata['phone'] . ""; ?></td>
                                                <td> <?php echo "" . $fdata['address'] . ""; ?></td>

                                                <td> <?php echo "" . $fdata['city'] . ""; ?></td>
                                                <td>
                                                    <?php echo "" . $fdata['zipcode'] . ""; ?>
                                                </td>

                                            </tr>
                                        <?php endwhile; ?>
                                    </tbody>

                                </table>

                            </div>

                        </div>
                        <!-- end card -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../libs/jquery/dist/jquery.min.js"></script>
    <script src="../libs/nicescroll/jquery.nicescroll.min.js"></script>
    <script src="../libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../libs/bootstrap/bootstrap.min.js"></script>
    <script src="../libs/PACE/pace.min.js"></script>
    <script src="../libs/chart.js/dist/Chart.min.js"></script>


    <!-- jquery-loading -->
    <script src="../libs/jquery-loading/dist/jquery.loading.min.js"></script>
    <!--datatables -->
    <script src="../libs/tables-datatables/dist/datatables.min.js"></script>



    <!-- octadmin Main Script -->
    <script src="../js/app.js"></script>

    <!-- datatable examples -->
    <script src="../js/table-datatable-example.js"></script>


</body>

</html>