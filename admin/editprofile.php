<?php
include('./webservices/connection/connection.php');
session_start();


?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Admin, Dashboard, Bootstrap" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>DASHBOARD</title>
    <link rel="stylesheet" href="../fonts/md-fonts/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../libs/animate.css/animate.min.css">
    <link rel="stylesheet" href="../libs/jquery-loading/dist/jquery.loading.min.css">

    <link id="pageStyle" rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../libs/tables-datatables/dist/datatables.min.css">
</head>

<body class="app sidebar-fixed aside-menu-off-canvas aside-menu-hidden header-fixed ">
    <?php include('header.php') ?>
    <div class="app-body">
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-accent-theme">
                            <div class="card-body">
                                <div class="row">

                                    <!-- Add new Admin -->

                                    <div class="col-sm-6">
                                        <div class="card">
                                            <div class="card-header text-theme">
                                                <strong>Add new Admin</strong>
                                                <small></small>
                                            </div>
                                            <div class="card-body">
                                                <form id="add_admin" method="post">

                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="name">name</label>
                                                                <input type="text" required name="name" class="form-control" id="name" placeholder="Enter Name">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="name">email</label>
                                                                <input type="email" required name="email" class="form-control" id="email" placeholder="Enter email">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="name"> password</label>
                                                                <input type="password" required name="password" class="form-control" id="password" placeholder="Enter  password">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="alert alert-success" id="success" role="alert" style="display: none;"></div>
                                                    <div class="alert alert-danger" id="warning" role="alert" style="display: none;"></div>
                                                    <button type="submit" class="btn btn-sm btn-primary">
                                                        <i class="fa fa-dot-circle-o"></i> Add Admin</button>
                                                </form>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-sm-6">
                                        <div class="card">
                                            <div class="card-header text-theme">
                                                <strong>change password</strong>
                                                <small></small>
                                            </div>
                                            <div class="card-body">
                                                <form id="update_password" method="post">
                                                    <input type="hidden" name="id" value="<?= $data1['id'] ?>" />

                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="name">old password</label>
                                                                <input type="password" required name="opassword" class="form-control" id="opassword" placeholder="Enter old password">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="name">new password</label>
                                                                <input type="password" required name="password" class="form-control" id="password" placeholder="Enter new password">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="alert alert-success" id="psuccess" role="alert" style="display: none;"></div>
                                                    <div class="alert alert-danger" id="pwarning" role="alert" style="display: none;"></div>
                                                    <button type="submit" class="btn btn-sm btn-primary">
                                                        <i class="fa fa-dot-circle-o"></i> Update</button>
                                                </form>
                                            </div>
                                        </div>

                                    </div>


                                </div>



                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../libs/jquery/dist/jquery.min.js"></script>
    <script src="../libs/nicescroll/jquery.nicescroll.min.js"></script>
    <script src="../libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../libs/bootstrap/bootstrap.min.js"></script>
    <script src="../libs/PACE/pace.min.js"></script>
    <script src="../libs/chart.js/dist/Chart.min.js"></script>
    <script src="../libs/jquery-loading/dist/jquery.loading.min.js"></script>
    <script src="../libs/tables-datatables/dist/datatables.min.js"></script>
    <script src="../js/app.js"></script>
    <script src="../js/table-datatable-example.js"></script>




    <script type="text/javascript">
        $(document).ready(function() {
            $("#update_password").on('submit', (function(e) {
                e.preventDefault();
                $.ajax({
                    url: "./webservices/ajax_changepassword.php",
                    type: "POST",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data) {
                        var obj = $.parseJSON(data);
                        if (obj.success == "success") {
                            $("#psuccess").show();
                            $("#psuccess").html("password changed sucessfully");
                            $("#psuccess").fadeOut(4000);
                            window.setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }
                        if (obj.success == "oldwrong") {
                            $("#pwarning").show();
                            $("#pwarning").html("old password is wrong");
                            $("#pwarning").fadeOut(4000);
                            window.setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }


                        if (obj.success == "fail") {
                            $("#pwarning").show();
                            $("#pwarning").html("try updating After Sometime");
                            $("#pwarning").fadeOut(4000);
                            window.setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }

                    }
                });
            }));
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#add_admin").on('submit', (function(e) {
                e.preventDefault();
                $.ajax({
                    url: "./webservices/ajax_addadmin.php",
                    type: "POST",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data) {
                        var obj = $.parseJSON(data);
                        if (obj.success == "success") {
                            $("#success").show();
                            $("#success").html("Admin added Successfully");
                            $("#success").fadeOut(4000);
                            window.setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }
                        if (obj.success == "fail") {
                            $("#warning").show();
                            $("#warning").html("Register After Sometime");
                            $("#warning").fadeOut(4000);
                            window.setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }
                        if (obj.success == "emailexist") {
                            $("#warning").show();
                            $("#warning").html("Email is taken");
                            $("#warning").fadeOut(4000);
                            window.setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }
                    }
                });
            }));
        });
    </script>

</body>

</html>