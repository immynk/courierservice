<?php
include('./webservices/connection/connection.php');
session_start();

$custCount = $db->query("SELECT count(*) as id from user WHERE type=0");
$fcustCount = $custCount->fetch();

$courierCount = $db->query("SELECT count(*) as id from user WHERE type=1");
$fcourierCount = $courierCount->fetch();

$empCount = $db->query("SELECT count(*) as id from user WHERE type=2");
$fempCount = $empCount->fetch();

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Admin, Dashboard, Bootstrap" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>DASHBOARD</title>
    <link rel="stylesheet" href="../fonts/md-fonts/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../libs/animate.css/animate.min.css">
    <link rel="stylesheet" href="../libs/jquery-loading/dist/jquery.loading.min.css">

    <link id="pageStyle" rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../libs/tables-datatables/dist/datatables.min.css">
</head>

<body class="app sidebar-fixed aside-menu-off-canvas aside-menu-hidden header-fixed ">
    <?php include('header.php') ?>
    <div class="app-body">
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-accent-theme">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="card card-accent-left-success">
                                            <a href="employee.php">
                                                <div class="card-body">

                                                    <div class="text-center">
                                                        <h1><?= $fempCount['id']; ?></h1>
                                                        <h6>Total Employee</h6>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card card-accent-left-success">
                                            <a href="customer.php">
                                                <div class="card-body">

                                                    <div class="text-center">
                                                        <h1><?= $fcustCount['id']; ?></h1>
                                                        <h6>Total customer</h6>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card card-accent-left-success">
                                            <a href="courier.php">
                                                <div class="card-body">

                                                    <div class="text-center">
                                                        <h1><?= $fcourierCount['id']; ?></h1>
                                                        <h6>Total Courier</h6>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>


                            </div>


                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../libs/jquery/dist/jquery.min.js"></script>
    <script src="../libs/nicescroll/jquery.nicescroll.min.js"></script>
    <script src="../libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../libs/bootstrap/bootstrap.min.js"></script>
    <script src="../libs/PACE/pace.min.js"></script>
    <script src="../libs/chart.js/dist/Chart.min.js"></script>
    <script src="../libs/jquery-loading/dist/jquery.loading.min.js"></script>
    <script src="../libs/tables-datatables/dist/datatables.min.js"></script>
    <script src="../js/app.js"></script>
    <script src="../js/table-datatable-example.js"></script>



</body>

</html>