<?php
include('./webservices/connection/connection.php');
session_start();


?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Admin, Dashboard, Bootstrap" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>DASHBOARD</title>
    <link rel="stylesheet" href="../fonts/md-fonts/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../libs/animate.css/animate.min.css">
    <link rel="stylesheet" href="../libs/jquery-loading/dist/jquery.loading.min.css">

    <link id="pageStyle" rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../libs/tables-datatables/dist/datatables.min.css">
</head>

<body class="app sidebar-fixed aside-menu-off-canvas aside-menu-hidden header-fixed ">
    <?php include('header.php') ?>
    <div class="app-body">
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-accent-theme">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="card card-accent-left-success">
                                            <a href="create_branch.php">
                                                <div class="card-body">

                                                    <div class="text-center">
                                                        <button class="btn btn-theme">Create Branch</button>

                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="card card-accent-left-success">
                                            <a href="branchlist.php">
                                                <div class="card-body">

                                                    <div class="text-center">
                                                    <button class="btn btn-theme">Branch List</button>
                                                       
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>

                                </div>


                            </div>


                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../libs/jquery/dist/jquery.min.js"></script>
    <script src="../libs/nicescroll/jquery.nicescroll.min.js"></script>
    <script src="../libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../libs/bootstrap/bootstrap.min.js"></script>
    <script src="../libs/PACE/pace.min.js"></script>
    <script src="../libs/chart.js/dist/Chart.min.js"></script>
    <script src="../libs/jquery-loading/dist/jquery.loading.min.js"></script>
    <script src="../libs/tables-datatables/dist/datatables.min.js"></script>
    <script src="../js/app.js"></script>
    <script src="../js/table-datatable-example.js"></script>



</body>

</html>