<?php
include('./webservices/connection/connection.php');

$id = $_SESSION['admin'];
if (!$id) {
    header("location:index.php");
}
$data = $db->query("SELECT * FROM admin WHERE id='$id'");

$data1 = $data->fetch(PDO::FETCH_ASSOC);


?>

<header class="app-header navbar">
    <div class="hamburger hamburger--arrowalt-r navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto">
        <div class="hamburger-box">
            <div class="hamburger-inner"></div>
        </div>
    </div>
    <!-- end hamburger -->
    <a class="navbar-brand" href="dashboard.php">
        <strong> Admin Panel</strong>

    </a>

    <ul class="nav navbar-nav ">

    <li class="nav-item dropdown">
            <a href="branch.php" class="btn btn-round btn-theme btn-sm">
                <span class=""> Branch
                   
                </span>
            </a>
        </li>

        <li class="nav-item dropdown">
            <a href="editprofile.php" class="btn btn-round btn-theme btn-sm">
                <span class=""> Edit profile
                    <i class="fa fa-gear"></i>
                </span>
            </a>
        </li>
        <li class="nav-item dropdown">
            <a href="logout.php" class="btn btn-round btn-theme btn-sm">
                <span class=""> Logout
                    <i class="fa fa-lock"></i>
                </span>
            </a>
        </li>
    </ul>
</header>