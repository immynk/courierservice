<?php
include('./webservices/connection/connection.php');
session_start();
$package_id = $_GET['id'];


?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Admin, Dashboard, Bootstrap" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>DASHBOARD</title>
    <link rel="stylesheet" href="./fonts/md-fonts/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="./fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="./libs/animate.css/animate.min.css">
    <link rel="stylesheet" href="./libs/jquery-loading/dist/jquery.loading.min.css">

    <link id="pageStyle" rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="./libs/tables-datatables/dist/datatables.min.css">
</head>

<body class="app sidebar-fixed aside-menu-off-canvas aside-menu-hidden header-fixed ">
    <?php include('header.php') ?>
    <div class="app-body">
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-accent-theme">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="card">
                                            <div class="card-header text-theme">
                                                <strong>update delivery status</strong>
                                                <small></small>
                                            </div>
                                            <div class="card-body">
                                                <form id="delivery_update" method="post">
                                                    <div class="alert alert-success" id="success" role="alert" style="display: none;"></div>
                                                    <div class="alert alert-danger" id="warning" role="alert" style="display: none;"></div>
                                                    <input type="hidden" name="id" value="<?= $_GET['id'] ?>" />
                                                    <select id="select" required name="delivery" class="form-control">
                                                        <option value="preparing">preparing</option>
                                                        <option value="onTransfer">onTransfer</option>
                                                        <option value="onBranch">onBranch</option>
                                                        <option value="delivered">delivered</option>
                                                        <option value="declined">declined</option>
                                                    </select>
                                                    <button type="submit" class="btn btn-sm btn-primary">
                                                        <i class="fa fa-dot-circle-o"></i> Update</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                   
                                </div>



                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="./libs/jquery/dist/jquery.min.js"></script>
    <script src="./libs/nicescroll/jquery.nicescroll.min.js"></script>
    <script src="./libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="./libs/bootstrap/bootstrap.min.js"></script>
    <script src="./libs/PACE/pace.min.js"></script>
    <script src="./libs/chart.js/dist/Chart.min.js"></script>
    <script src="./libs/jquery-loading/dist/jquery.loading.min.js"></script>
    <script src="./libs/tables-datatables/dist/datatables.min.js"></script>
    <script src="./js/app.js"></script>
    <script src="./js/table-datatable-example.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#delivery_update").on('submit', (function(e) {
                e.preventDefault();
                $.ajax({
                    url: "./webservices/ajax_updatecorierdelivery.php",
                    type: "POST",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data) {
                        var obj = $.parseJSON(data);
                        if (obj.success == "success") {
                            $("#success").show();
                            $("#success").html("Package status updated successfully");
                            $("#success").fadeOut(4000);
                            window.setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }
                        if (obj.success == "fail") {
                            $("#warning").show();
                            $("#warning").html("try updating After Sometime");
                            $("#warning").fadeOut(4000);
                            window.setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }

                    }
                });
            }));
        });
    </script>



</body>

</html>