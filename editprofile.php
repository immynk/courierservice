<?php
include('./webservices/connection/connection.php');
session_start();

$corier_id = $_SESSION['id'];
$fetchVeh = $db->query("SELECT * from vehicle WHERE user_id='$corier_id'");
$fVeh = $fetchVeh->fetch(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Admin, Dashboard, Bootstrap" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>DASHBOARD</title>
    <link rel="stylesheet" href="./fonts/md-fonts/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="./fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="./libs/animate.css/animate.min.css">
    <link rel="stylesheet" href="./libs/jquery-loading/dist/jquery.loading.min.css">

    <link id="pageStyle" rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="./libs/tables-datatables/dist/datatables.min.css">
</head>

<body class="app sidebar-fixed aside-menu-off-canvas aside-menu-hidden header-fixed ">
    <?php include('header.php') ?>
    <div class="app-body">
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-accent-theme">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="card">
                                            <div class="card-header text-theme">
                                                <strong>update Profile</strong>
                                                <small></small>
                                            </div>
                                            <div class="card-body">
                                                <form id="update_info" method="post">
                                                    <input type="hidden" name="id" value="<?= $data1['id'] ?>" />
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="name">name</label>
                                                                <input type="text" value="<?= $data1['name'] != '' ? $data1['name'] : ''; ?>" required name="name" class="form-control" id="name" placeholder="Enter name">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="name">email</label>
                                                                <input type="email" value="<?= $data1['email'] != '' ? $data1['email'] : ''; ?>" required name="email" class="form-control" id="email" placeholder="Enter email">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="name">phone</label>
                                                                <input type="text" value="<?= $data1['phone'] != '' ? $data1['phone'] : ''; ?>" required name="phone" class="form-control" id="phone" placeholder="Enter phone">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="name">address</label>
                                                                <input type="text" value="<?= $data1['address'] != '' ? $data1['address'] : ''; ?>" required name="address" class="form-control" id="address" placeholder="Enter address">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="name">city</label>
                                                                <input type="text" value="<?= $data1['city'] != '' ? $data1['city'] : ''; ?>" required name="city" class="form-control" id="city" placeholder="Enter city">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="name">zipcode</label>
                                                                <input type="text" value="<?= $data1['zipcode'] != '' ? $data1['zipcode'] : ''; ?>" required name="zipcode" class="form-control" id="zipcode" placeholder="Enter zipcode">
                                                            </div>
                                                        </div>
                                                    </div> -->
                                                    <div class="alert alert-success" id="success" role="alert" style="display: none;"></div>
                                                    <div class="alert alert-danger" id="warning" role="alert" style="display: none;"></div>


                                                    <button type="submit" class="btn btn-sm btn-primary">
                                                        <i class="fa fa-dot-circle-o"></i> Update</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="card">
                                            <div class="card-header text-theme">
                                                <strong>change password</strong>
                                                <small></small>
                                            </div>
                                            <div class="card-body">
                                                <form id="update_password" method="post">
                                                    <input type="hidden" name="id" value="<?= $data1['id'] ?>" />

                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="name">old password</label>
                                                                <input type="password" required name="opassword" class="form-control" id="opassword" placeholder="Enter old password">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="name">new password</label>
                                                                <input type="password" required name="password" class="form-control" id="password" placeholder="Enter new password">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="alert alert-success" id="psuccess" role="alert" style="display: none;"></div>
                                                    <div class="alert alert-danger" id="pwarning" role="alert" style="display: none;"></div>
                                                    <button type="submit" class="btn btn-sm btn-primary">
                                                        <i class="fa fa-dot-circle-o"></i> Update</button>
                                                </form>
                                            </div>
                                        </div>
                                        <?php
                                        if ($data1['corier_status'] == 1 && $userType == 1) {
                                        ?>
                                            <div class="card">
                                                <div class="card-header text-theme">
                                                    <strong>change vehicle details</strong>
                                                    <small></small>
                                                </div>
                                                <div class="card-body">
                                                    <form id="update_veh" method="post">
                                                        <input type="hidden" name="id" value="<?= $data1['id'] ?>" />

                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <label for="name">vehicle name</label>
                                                                    <input type="text" value="<?= $fVeh['vehicle_name'] != '' ? $fVeh['vehicle_name'] : ''; ?>" required name="vehicle_name" class="form-control" id="vehicle_name" placeholder="Enter vehicle name">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <label for="name">licence number</label>
                                                                    <input type="text" value="<?= $fVeh['licence'] != '' ? $fVeh['licence'] : ''; ?>" required name="licence" class="form-control" id="licence" placeholder="Enter licence number">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <label for="name">number plate</label>
                                                                    <input type="text" value="<?= $fVeh['number_plate'] != '' ? $fVeh['number_plate'] : ''; ?>" required name="number_plate" class="form-control" id="number_plate" placeholder="Enter  number plate">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="alert alert-success" id="vsuccess" role="alert" style="display: none;"></div>
                                                        <div class="alert alert-danger" id="vwarning" role="alert" style="display: none;"></div>
                                                        <button type="submit" class="btn btn-sm btn-primary">
                                                            <i class="fa fa-dot-circle-o"></i> Update</button>
                                                    </form>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>

                                </div>



                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="./libs/jquery/dist/jquery.min.js"></script>
    <script src="./libs/nicescroll/jquery.nicescroll.min.js"></script>
    <script src="./libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="./libs/bootstrap/bootstrap.min.js"></script>
    <script src="./libs/PACE/pace.min.js"></script>
    <script src="./libs/chart.js/dist/Chart.min.js"></script>
    <script src="./libs/jquery-loading/dist/jquery.loading.min.js"></script>
    <script src="./libs/tables-datatables/dist/datatables.min.js"></script>
    <script src="./js/app.js"></script>
    <script src="./js/table-datatable-example.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#update_info").on('submit', (function(e) {
                e.preventDefault();
                $.ajax({
                    url: "./webservices/ajax_updateprofile.php",
                    type: "POST",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data) {
                        var obj = $.parseJSON(data);
                        if (obj.success == "success") {
                            $("#success").show();
                            $("#success").html("profile updated successfully");
                            $("#success").fadeOut(4000);
                            window.setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }
                        if (obj.success == "emailexist") {
                            $("#warning").show();
                            $("#warning").html("email is taken by someone");
                            $("#warning").fadeOut(4000);
                            window.setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }
                        if (obj.success == "phoneexist") {
                            $("#warning").show();
                            $("#warning").html("phone is taken by someone");
                            $("#warning").fadeOut(4000);
                            window.setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }


                        if (obj.success == "fail") {
                            $("#warning").show();
                            $("#warning").html("try updating After Sometime");
                            $("#warning").fadeOut(4000);
                            window.setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }

                    }
                });
            }));
        });
    </script>


    <script type="text/javascript">
        $(document).ready(function() {
            $("#update_password").on('submit', (function(e) {
                e.preventDefault();
                $.ajax({
                    url: "./webservices/ajax_changepassword.php",
                    type: "POST",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data) {
                        var obj = $.parseJSON(data);
                        if (obj.success == "success") {
                            $("#psuccess").show();
                            $("#psuccess").html("password changed sucessfully");
                            $("#psuccess").fadeOut(4000);
                            window.setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }
                        if (obj.success == "oldwrong") {
                            $("#pwarning").show();
                            $("#pwarning").html("old password is wrong");
                            $("#pwarning").fadeOut(4000);
                            window.setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }


                        if (obj.success == "fail") {
                            $("#pwarning").show();
                            $("#pwarning").html("try updating After Sometime");
                            $("#pwarning").fadeOut(4000);
                            window.setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }

                    }
                });
            }));
        });
    </script>


    <script type="text/javascript">
        $(document).ready(function() {
            $("#update_veh").on('submit', (function(e) {
                e.preventDefault();
                $.ajax({
                    url: "./webservices/ajax_changevehicle.php",
                    type: "POST",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data) {
                        var obj = $.parseJSON(data);
                        if (obj.success == "success") {
                            $("#vsuccess").show();
                            $("#vsuccess").html("vehicle updated sucessfully");
                            $("#vsuccess").fadeOut(4000);
                            window.setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }

                        if (obj.success == "fail") {
                            $("#vwarning").show();
                            $("#vwarning").html("try updating After Sometime");
                            $("#vwarning").fadeOut(4000);
                            window.setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }

                    }
                });
            }));
        });
    </script>
</body>

</html>