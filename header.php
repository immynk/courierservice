<?php
include('./webservices/connection/connection.php');

$id = $_SESSION['id'];
if (!$id) {
    header("location:index.php");
}

$data = $db->query("SELECT * FROM user WHERE id='$id'");

$data1 = $data->fetch(PDO::FETCH_ASSOC);
$userType = $data1['type'];



?>

<header class="app-header navbar">
    <div class="hamburger hamburger--arrowalt-r navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto">
        <div class="hamburger-box">
            <div class="hamburger-inner"></div>
        </div>
    </div>
    <!-- end hamburger -->
    <a class="navbar-brand" href="dashboard.php">
        <strong> <?php echo $data1['name'] ?><sup>dashboard</sup></strong>

    </a>

    <ul class="nav navbar-nav ">
        <!-- <li class="nav-item dropdown">
            <span class="">
                <?php echo $data1['name'] ?>
            </span>
        </li> -->
        <?php
        if ($userType == 0) {
        ?>
            <li class="nav-item ">
                <a href="mypackage.php" class="btn btn-round btn-theme btn-sm">
                    <span class=""> Sending package
                    </span>
                </a>
            </li>
            <li class="nav-item ">
                <a href="recievingpackage.php" class="btn btn-round btn-theme btn-sm">
                    <span class=""> Recieving package
                    </span>
                </a>
            </li>
        <?php } ?>

        <?php
        if ($userType == 2) {
        ?>
            <li class="nav-item dropdown">
                <a href="courier_package.php" class="btn btn-round btn-theme btn-sm">
                    <span class="">assigned package to cuorier
                    </span>
                </a>
            </li>
            <li class="nav-item dropdown">
                <a href="courier_list.php" class="btn btn-round btn-theme btn-sm">
                    <span class=""> Cuorier
                    </span>
                </a>
            </li>
        <?php
        }
        ?>
        <li class="nav-item dropdown">
            <a href="editprofile.php" class="btn btn-round btn-theme btn-sm">
                <span class=""> Edit profile
                    <i class="fa fa-gear"></i>
                </span>
            </a>
        </li>
        <li class="nav-item dropdown">
            <a href="logout.php" class="btn btn-round btn-theme btn-sm">
                <span class=""> Logout
                    <i class="fa fa-lock"></i>
                </span>
            </a>
        </li>
    </ul>
</header>