<?php
include('./webservices/connection/connection.php');
session_start();
if (isset($_SESSION['id'])) {
    header("location:dashboard.php");
}
$branch = $db->query("SELECT * FROM `branch`");
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Admin, Dashboard, Bootstrap" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />


    <title>Register</title>


    <meta name="theme-color" content="#ffffff">

    <!-- fonts -->

    <link rel="stylesheet" href="./fonts/md-fonts/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="./fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!-- animate css -->
    <link rel="stylesheet" href="./libs/animate.css/animate.min.css">

    <link rel="stylesheet" href="./libs/jquery-loading/dist/jquery.loading.min.css">

    <link id="pageStyle" rel="stylesheet" href="./css/style.css">



</head>

<body>
    <section class="container-pages">



        <div class="card pages-card col-lg-4 col-md-6 col-sm-6">
            <div class="card-body ">
                <div class="h4 text-center text-theme"><strong>Register</strong></div>
                <div class="small text-center text-dark"> Dashboard Register</div>

                <form id="register_form" method="post">
                    <div class="form-group">
                        <div class="input-group">
                            <select required name="usertype" style="width: 100%;">
                                <option value="0">Customer Register</option>
                                <option value="1">Courier Register</option>
                                <option value="2">Employee Register</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon text-theme"><i class="fa fa-user"></i>
                            </span>
                            <input type="text" id="name" required name="name" class="form-control" placeholder=" Your Name">
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon text-theme"><i class="fa fa-envelope"></i>
                            </span>
                            <input type="email" id="email" required name="email" class="form-control" placeholder="Email">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon text-theme"><i class="fa fa-phone"></i>
                            </span>
                            <input type="text" id="phone" required name="phone" class="form-control" placeholder="phone">
                        </div>
                    </div>




                    <div class="form-group">
                        <label for="name">Select branch</label>
                        <div class="input-group">
                            <select required name="branch" style="width: 100%;">
                                <?php while ($fdata = $branch->fetch(PDO::FETCH_ASSOC)) : ?>
                                    <option value="<?= $fdata['id']; ?>"><?= $fdata['city'] ?></option>

                                <?php endwhile; ?>
                            </select>
                        </div>
                    </div>







                    <!-- <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon text-theme"><i class="fa fa-building-o"></i>
                            </span>
                            <input type="text" id="city" required name="city" class="form-control" placeholder="city">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon text-theme"><i class="fa fa-file-zip-o"></i>
                            </span>
                            <input type="text" id="zipcode" required name="zipcode" class="form-control" placeholder="zip code">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon text-theme"><i class="fa fa-address-card"></i>
                            </span>
                            <input type="text" id="address" required name="address" class="form-control" placeholder=" Address">
                        </div>
                    </div> -->

                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon text-theme"><i class="fa fa-asterisk"></i></span>
                            <input type="password" id="password" required name="password" class="form-control" placeholder="Password">

                        </div>
                    </div>


                    <div class="alert alert-success" id="success" role="alert" style="display: none;"></div>
                    <div class="alert alert-danger" id="warning" role="alert" style="display: none;"></div>
                    <div class="form-group form-actions">
                        <button type="submit" class="btn  btn-theme login-btn "> Register </button>
                    </div>
                    <div class="text-center">
                        <a href="index.php" id="forgetshow" class="text-theme">Already Register ?</a>
                    </div>
                </form>




            </div>
            <!-- end card-body -->
        </div>
        <!-- end card -->
    </section>
    <!-- end section container -->
    <div class="half-circle"></div>
    <div class="small-circle"></div>


    <!-- end mybutton -->



    <!-- Bootstrap and necessary plugins -->
    <script src="./libs/jquery/dist/jquery.min.js"></script>
    <script src="./libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="./libs/bootstrap/bootstrap.min.js"></script>
    <script src="./libs/PACE/pace.min.js"></script>
    <script src="./libs/chart.js/dist/Chart.min.js"></script>
    <script src="./libs/nicescroll/jquery.nicescroll.min.js"></script>

    <script src="./libs/jquery-knob/dist/jquery.knob.min.js"></script>


    <!-- jquery-loading -->
    <script src="./libs/jquery-loading/dist/jquery.loading.min.js"></script>
    <!-- octadmin Main Script -->
    <script src="./js/app.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#register_form").on('submit', (function(e) {
                e.preventDefault();
                $.ajax({
                    url: "./webservices/ajax_userregister.php",
                    type: "POST",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data) {
                        var obj = $.parseJSON(data);
                        if (obj.success == "success") {
                            $("#success").show();
                            $("#success").html("Registration Successfully");
                            $("#success").fadeOut(4000);
                            window.setTimeout(function() {
                                location.href = "index.php"
                            }, 3000);
                        }
                        if (obj.success == "fail") {
                            $("#warning").show();
                            $("#warning").html("Register After Sometime");
                            $("#warning").fadeOut(4000);
                            window.setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }
                        if (obj.success == "emailexist") {
                            $("#warning").show();
                            $("#warning").html("Email is taken");
                            $("#warning").fadeOut(4000);
                            window.setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }
                    }
                });
            }));
        });
    </script>

</body>

</html>