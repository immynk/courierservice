<?php
include('./webservices/connection/connection.php');
session_start();
$to_id = $_GET['id'];
$id = $_SESSION['id'];
$assign_id = $db->query("SELECT id FROM user WHERE type=2 ORDER BY RAND() LIMIT 1");
$fassignid = $assign_id->fetch(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Admin, Dashboard, Bootstrap" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Create Transaction</title>
    <link rel="stylesheet" href="./fonts/md-fonts/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="./fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="./libs/animate.css/animate.min.css">
    <link rel="stylesheet" href="./libs/jquery-loading/dist/jquery.loading.min.css">

    <link id="pageStyle" rel="stylesheet" href="./css/style.css">

</head>

<body class="app sidebar-fixed aside-menu-off-canvas aside-menu-hidden header-fixed ">
    <?php include('header.php') ?>

    <div class="app-body">
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-accent-theme">
                            <div class="card-body">
                                <form id="create_package" method="post">
                                    <div class="card">
                                        <div class="card-header text-theme">
                                            <strong>Create Package</strong>
                                            <small></small>
                                        </div>
                                        <input type="hidden" name="to_id" value="<?= $to_id  ?>">
                                        <input type="hidden" name="user_id" value="<?= $id  ?>">
                                        <input type="hidden" name="assign_id" value="<?php echo $fassignid['id'] ?>">
                                     
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="name">Title</label>
                                                        <input type="text" required name="title" class="form-control" id="title" placeholder="Enter Tile">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="name">Description</label>
                                                        <input type="text" required name="description" class="form-control" id="description" placeholder="Enter Description">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="name">Delivery Address</label>
                                                        <input type="text" name="address" required class="form-control" id="address" placeholder="Enter Address">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="name">Delivery Date</label>
                                                        <input type="date" name="delivery_date" required class="form-control" id="delivery_date" placeholder="Delivery Date">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="name">Location</label>
                                                        <input type="text" name="location" required class="form-control" id="location" placeholder="Enter location">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="name">Weight</label>
                                                        <input type="number" name="weight" required class="form-control" id="weight" placeholder="Enter Weight">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="name">Type</label>
                                                        <input type="text" name="type" required class="form-control" id="type" placeholder="Enter type">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="alert alert-success" id="success" role="alert" style="display: none;"></div>
                                            <div class="alert alert-danger" id="warning" role="alert" style="display: none;"></div>
                                            <div class="row">
                                                <button type="submit" class="btn btn-theme btn-sm"><i class="fa fa-dot-circle-o"></i> Create Package</button>
                                            </div>
                                            <!--/.row-->
                                        </div>
                                        <!-- end card-body -->
                                    </div>
                                </form>


                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="./libs/jquery/dist/jquery.min.js"></script>
    <script src="./libs/nicescroll/jquery.nicescroll.min.js"></script>
    <script src="./libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="./libs/bootstrap/bootstrap.min.js"></script>
    <script src="./libs/PACE/pace.min.js"></script>
    <script src="./libs/chart.js/dist/Chart.min.js"></script>


    <!-- jquery-loading -->
    <script src="./libs/jquery-loading/dist/jquery.loading.min.js"></script>



    <!-- octadmin Main Script -->
    <script src="./js/app.js"></script>

    <!-- datatable examples -->
    <script src="./js/table-datatable-example.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#create_package").on('submit', (function(e) {
                e.preventDefault();
                $.ajax({
                    url: "./webservices/ajax_package.php",
                    type: "POST",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data) {
                        var obj = $.parseJSON(data);
                        if (obj.success == "success") {
                            $("#success").show();
                            $("#success").html("Package created successfully");
                            $("#success").fadeOut(4000);
                            window.setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }
                        if (obj.success == "fail") {
                            $("#warning").show();
                            $("#warning").html("create pacage After Sometime");
                            $("#warning").fadeOut(4000);
                            window.setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }

                    }
                });
            }));
        });
    </script>


</body>

</html>