<?php 
session_start();
 if(isset($_SESSION['id']))
    {
      header("location:dashboard.php");
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Admin, Dashboard, Bootstrap" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    
    <title>Login</title>


    <meta name="theme-color" content="#ffffff">

    <!-- fonts -->
   
    <link rel="stylesheet" href="./fonts/md-fonts/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="./fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!-- animate css -->
    <link rel="stylesheet" href="./libs/animate.css/animate.min.css">

     <link rel="stylesheet" href="./libs/jquery-loading/dist/jquery.loading.min.css">
    
    <link id="pageStyle" rel="stylesheet" href="./css/style.css">

   

</head>

<body>
    <section class="container-pages">
       
     

        <div class="card pages-card col-lg-4 col-md-6 col-sm-6">
            <div class="card-body ">
                <div class="h4 text-center text-theme"><strong>Login</strong></div>
                <div class="small text-center text-dark"> Dashboard Login</div>
               
                    <form id="login_form" method="post" >
                    <div class="form-group">
                        <div class="input-group">
                            <select required name="usertype" style="width: 100%;">
                                <option value="0">Customer Login</option>
                                <option value="1">Courier Login</option>
                                <option value="2">Employee Login</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon text-theme"><i class="fa fa-envelope"></i>
                            </span>
                            <input type="email" id="email" required name="email" class="form-control" placeholder="Email">
                        </div>
                    </div>
                        
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon text-theme"><i class="fa fa-asterisk"></i></span>
                                <input type="password" id="password" required name="password" class="form-control" placeholder="Password">

                            </div>
                        </div>
                        <div class="alert alert-success" id = "success" role="alert" style="display: none;"></div>
                        <div class="alert alert-danger" id = "warning" role="alert" style="display: none;"></div>
                        <div class="form-group form-actions">
                            <button type="submit" class="btn  btn-theme login-btn ">   Login   </button>
                        </div>
                        <div class="text-center">
                            <a href="register.php" id="forgetshow" class="text-theme">New User ?</a>
                        </div>
                    </form>

                  
                    
              
            </div>
            <!-- end card-body -->
        </div>
        <!-- end card -->
    </section>
    <!-- end section container -->
    <div class="half-circle"></div>
    <div class="small-circle"></div>

    
         <!-- end mybutton -->

    
   
         <!-- Bootstrap and necessary plugins -->
    <script src="./libs/jquery/dist/jquery.min.js"></script>
    <script src="./libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="./libs/bootstrap/bootstrap.min.js"></script>
    <script src="./libs/PACE/pace.min.js"></script>
    <script src="./libs/chart.js/dist/Chart.min.js"></script>
    <script src="./libs/nicescroll/jquery.nicescroll.min.js"></script>

    <script src="./libs/jquery-knob/dist/jquery.knob.min.js"></script>

        
    <!-- jquery-loading -->
    <script src="./libs/jquery-loading/dist/jquery.loading.min.js"></script>
    <!-- octadmin Main Script -->
    <script src="./js/app.js"></script>

    <script type="text/javascript">
    $(document).ready(function()
    {
        $("#login_form").on('submit',(function(e)
        {
            e.preventDefault();
            $.ajax({
                url: "./webservices/ajax_userlogin.php",
                type: "POST",
                data:  new FormData(this),
                contentType: false,
                cache: false,
                processData:false,
                success: function(data){
                    var obj = $.parseJSON(data);
                    if(obj.success=="success")
                    {
                        $("#success").show();                       
                        $("#success").html("Login Successfully");
                        $("#success").fadeOut(4000);
                         window.setTimeout(function(){location.href="dashboard.php"},3000);    
                    }
                      if(obj.success=="fail")
                    {
                        $("#warning").show();                       
                        $("#warning").html("Please enter valid email or password");
                        $("#warning").fadeOut(4000);
                        window.setTimeout(function(){location.reload()},3000);
                    }
                }
            });
        }));
    });
  </script>

</body>

</html>