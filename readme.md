customer X logs in -> creates a transaction to another customer Y with a package
employee A logs in -> verifies the payment status, assigns the package to courier C with vehicle V
courier C delivers the package to customer Y, customer Y can issue a complaint if he/she wants to. This complaint can be seen by any employees that are not couriers. Customers can message employees. regardless whether they sent or recieved packages


Shipping Company Data Management System
1. Sending and receiving a package to a registered customer (by a customer)
a. Recipient should be chosen among a list of possible customers.
b. Description about the package should be described; package type, payment side,
delivery type and etc.
c. Each customer can see list of packages sent or received by her along with current
status of the packages: preparing, onTransfer, onBranch, delivered, declined.
d. Customers can see the transportation process of their package listing arrival and
departure of the package in each stop or hub.
e. Approving and finalizing the package.
f. For broken or damaged packages, customers can file a report about it and decline
the shipping.
g. Each information about the report should be kept track of such as date, time,
courier delivering the package and etc.
2. Managing Reports (by a customer service ie employee)
a. List of all reports in the pool: ongoing, finished, onOthers, waiting.
b. Choosing any reports in the pool to manage
c. Talking to customer via messages (sending, receiving through a chatView )
d. Finalizing the reports : positive or negative



Courier:
Couriers should be able to log in to the system by entering their Employee Identification Number and their password.
Couriers will be able to access to their profile page, which includes several fields like name, identification number, e-mail address and phone number.
Couriers will be able to change their passwords after they log in.
Couriers will be able to see which vehicles they are assigned to.
Couriers will be able to see packages to be delivered, the identification number of each individual package, the senders and receivers of said packages, the date package is accepted by the company and the status of the transaction. Couriers can also see the details of a package (weight etc.) as well.
Couriers will be able to remove delivery orders from the delivery list after the package is distributed.
Couriers will be able to add a new record after the package is delivered. The record should contain delivery date and the receiver’s name.
Employee
Employee should be able to log in to the system by entering their Employee Identification Number and their password.
Employees will be able to access to their profile page, which includes several fields like name, identification number, the branch they are assigned to, e-mail address and phone number.
Employees will be able to change their passwords after they log in.
Employees will be able to see the packages waiting for the delivery in the branch they are assigned to, the packages that are in delivery by the couriers and the senders and the receivers of the said packages.
The employees should be able to see the identification number of each individual package in the branch, the date package is accepted to the branch and the status of the transaction.
The employees should be able to see the registered complaints, the type of the complaint and which customer filed them.



Customer
Customer should be able to log in to the system by entering their Customer Identification Number and their password.
Customers should be able to see their profile page, which includes several fields like name, address and phone number.
Customers will be able to change their passwords after they log in.
The customers should be able to register complaints after logging in to the system. 
Customers should be able to see the status of the transaction they are waiting for, the payment, type of delivery, the date the transaction is filed in and the current status.
Customers should be able to see the identification number of the package and the weight of the package as well as other details. 
Registered customers can see and send packages to other registered customers.


also, a couple of things extra, quite easy to implement
employees need a reports page which show the following:

Total Number of Delivered Packages for Couriers and
Total Number of Vehicles Available for Branches

and 2 views for customer and courier:
Waiting Packages View for Couriers
Registered Customer View for Customers


also need the following

Triggers
When a courier is added to the system, a vehicle will be assigned according to the couriers licence type.
When a courier delivers a package, the status of the transaction and the package will be updated as well.
When a customer logs is created, the default status of being registered is set to 0. If the customer changes their registry status, the views are generated automatically
Constraints
The system requires a login.
Only registered customers can see other registered customers and send packages to them.
Couriers can’t see complaints about packages they delivered.
All integer and float values mentioned in entities must be greater or equal to zero except for payment in Transaction and weight in Package which must be greater than zero.
Drop time of a delivery cannot be earlier than the date of a transaction.
A courier cannot be added to a branch that does not have a vehicle the courier can operate.
A courier cannot be entered to the system without the drivers_licence_type.
Complaint types that are related to the package’s physical status can only be entered after it is delivered.


# CHANGES
Ok I tested it thoroughly and its amazing. But I want a small change if possible. Can you add an admin user who can create/edit branches and assign employees and couriers to them. (so employees cannot choose their branches). Admin should also see through an SQL query a report of how many packages a branch has delivered, and how many have complaints.

- changes user can select city from dropdown of created branch
- assign package to employee city wise

-  assign emp and courier to branches to get list of courier on employee panel with  branch assigned
- get completed deliver list of courier with branch and report of courier