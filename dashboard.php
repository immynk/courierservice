<?php
include('./webservices/connection/connection.php');
session_start();
$id = $_SESSION['id'];
$customer = $db->query("SELECT * FROM `user` WHERE type=0 AND id!='$id'");
$mypackage = $db->query("SELECT * FROM `package` WHERE assign_id='$id'");

$mydeliver = $db->query("SELECT * FROM `corier_package` WHERE to_id='$id'");

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Admin, Dashboard, Bootstrap" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>DASHBOARD</title>
    <link rel="stylesheet" href="./fonts/md-fonts/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="./fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="./libs/animate.css/animate.min.css">
    <link rel="stylesheet" href="./libs/jquery-loading/dist/jquery.loading.min.css">

    <link id="pageStyle" rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="./libs/tables-datatables/dist/datatables.min.css">
</head>

<body class="app sidebar-fixed aside-menu-off-canvas aside-menu-hidden header-fixed ">
    <?php include('header.php') ?>
    <div class="app-body">
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-accent-theme">
                            <div class="card-body">
                                <?php
                                if ($userType == 2) {
                                ?>
                                    <h4 class="text-theme">Package assigned to me </h4>
                                <?php
                                }
                                ?>
                                <?php
                                if ($userType == 0) {
                                ?>
                                    <h4 class="text-theme">Customer List </h4>
                                <?php
                                }
                                ?>
                                <?php
                                if ($userType == 1) {
                                ?>
                                    <h4 class="text-theme">my delivery list </h4>
                                <?php
                                }
                                ?>
                                <br />
                                <!-- Customer dashboard -->
                                <?php
                                if ($userType == 0) {
                                ?>
                                    <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>City</th>
                                                <th>Address</th>
                                                <th>Create Package</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>City</th>
                                                <th>Address</th>
                                                <th>Create Package</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <?php while ($fdata = $customer->fetch(PDO::FETCH_ASSOC)) : ?>
                                                <tr>
                                                    <td> <?php echo "" . $fdata['name'] . ""; ?></td>
                                                    <td> <?php echo "" . $fdata['email'] . ""; ?></td>
                                                    <td> <?php echo "" . $fdata['phone'] . ""; ?></td>
                                                    <td> <?php echo "" . $fdata['city'] . ""; ?></td>
                                                    <td> <?php echo "" . $fdata['address'] . ""; ?></td>
                                                    <td>
                                                        <a href="create_package.php?id=<?php echo "" . $fdata['id'] . ""; ?>" data-toggle="tooltip" data-original-title="Create">
                                                            <i style="font-size: 35px;" class="fa fa-telegram text-success"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php endwhile; ?>
                                        </tbody>
                                    </table>
                                <?php
                                }
                                ?>
                                <!-- End Customer dashboard -->
                                <!-- Employee dashboard -->
                                <?php
                                if ($userType == 2) {
                                ?>
                                    <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sender Detail </th>
                                                <th>Deliver Detail</th>
                                                <th>title</th>
                                                <th>weight</th>
                                                <th>Delivery Address</th>
                                                <th>Delivery Date</th>
                                                <th>Delivery status</th>
                                                <th>track id </th>
                                                <th>Payment status</th>
                                                <th>Action </th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Sender Detail</th>
                                                <th>Deliver Detail</th>
                                                <th>title</th>
                                                <th>weight</th>
                                                <th>Delivery Address</th>
                                                <th>Delivery Date</th>
                                                <th>Delivery status</th>
                                                <th>track id </th>
                                                <th>Payment status</th>
                                                <th>Action </th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <?php while ($fdata = $mypackage->fetch(PDO::FETCH_ASSOC)) : ?>
                                                <?php
                                                $senderID = $fdata['user_id'];
                                                $recieverID = $fdata['to_id'];
                                                $assignID = $fdata['assign_id'];
                                                $paymentID = $fdata['hash_id'];
                                                $sender = $db->query("SELECT * FROM `user` WHERE id='$senderID'");
                                                $fsender = $sender->fetch(PDO::FETCH_ASSOC);
                                                $reciever = $db->query("SELECT * FROM `user` WHERE id='$recieverID'");
                                                $freciever = $reciever->fetch(PDO::FETCH_ASSOC);
                                                $payment = $db->query("SELECT * FROM `payment` WHERE package_id='$paymentID'");
                                                $fpayment = $payment->fetch(PDO::FETCH_ASSOC);
                                                ?>
                                                <tr>
                                                    <td>

                                                        <button type="button" class=" badge badge-boxed badge-success btn-xs start_chat" data-touserid="<?php echo "" . $fsender['id'] . ""; ?>" data-tousername=" <?php echo "" . $fsender['name'] . ""; ?>"> <?php echo "" . $fsender['name'] . ""; ?></button>

                                                        phone:<?php echo "" . $fsender['phone'] . ""; ?>
                                                    </td>
                                                    <td> <?php echo "" . $freciever['name'] . ""; ?>
                                                        phone:<?php echo "" . $freciever['phone'] . ""; ?>
                                                    </td>
                                                    <td> <?php echo "" . $fdata['title'] . ""; ?></td>
                                                    <td> <?php echo "" . $fdata['weight'] . ""; ?></td>
                                                    <td> <?php echo "" . $fdata['address'] . ""; ?></td>
                                                    <td>
                                                        <?php echo "" . $fdata['delivery_date'] . ""; ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        if ($fdata['status'] == "declined") {
                                                        ?>
                                                            <span class="badge badge-boxed badge-danger"> <?php echo "" . $fdata['status'] . ""; ?></span>
                                                        <?php
                                                        } else {
                                                        ?>
                                                            <span class="badge badge-boxed badge-info"> <?php echo "" . $fdata['status'] . ""; ?></span>
                                                        <?php
                                                        }
                                                        ?>

                                                    </td>
                                                    <td>
                                                        <?php echo "" . $fdata['hash_id'] . ""; ?>
                                                    </td>
                                                    <td>

                                                        <?php
                                                        if ($fpayment['status'] == "pending") {
                                                        ?>
                                                            <span class=" badge badge-boxed badge-danger"> <?php echo "" . $fpayment['status'] . ""; ?></span>
                                                        <?php
                                                        } else {
                                                        ?>
                                                            <span class=" badge badge-boxed badge-success"> <?php echo "" . $fpayment['status'] . ""; ?></span>
                                                        <?php
                                                        }
                                                        ?>

                                                    </td>
                                                    <td>
                                                        <a href="updatepackage.php?id=<?php echo "" . $fdata['id'] . ""; ?>"> <span class="badge badge-boxed badge-success"> Edit</span></a>
                                                    </td>
                                                </tr>
                                            <?php endwhile; ?>
                                        </tbody>
                                    </table>
                                <?php
                                }
                                ?>
                                <!-- End Employee dashboard -->
                                <!-- start corier dashboard -->


                                <?php
                                if ($data1['corier_status'] == 0 && $userType == 1) {
                                ?>
                                    <div class="card">
                                        <div class="card-header text-theme">
                                            <strong>update vehicle to see delivery list</strong>
                                            <small></small>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <form id="update_vehicleinfo" method="post">
                                            <input type="hidden" name="id" value="<?= $data1['id'] ?>" />
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="name">vehicle name:</label>
                                                        <input type="text" required name="vehicle_name" class="form-control" id="vehicle_name" placeholder="Enter name">
                                                        <small>use comma(,) to seprate multiple vehicle name</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="name">Number plate:</label>
                                                        <input type="text" required name="number_plate" class="form-control" id="number_plate" placeholder="Enter number plate">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="name">Licence number:</label>
                                                        <input type="text" required name="licence" class="form-control" id="licence" placeholder="Enter licence number">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="alert alert-success" id="success" role="alert" style="display: none;"></div>
                                            <div class="alert alert-danger" id="warning" role="alert" style="display: none;"></div>
                                            <button type="submit" class="btn btn-sm btn-primary">
                                                <i class="fa fa-dot-circle-o"></i> Update</button>
                                        </form>
                                    </div>
                                <?php
                                }
                                ?>


                                <?php
                                if ($data1['corier_status'] == 1 && $userType == 1) {
                                ?>
                                    <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable" width="100%">
                                        <thead>
                                            <tr>
                                                <th>track id</th>
                                                <th>deliver name </th>
                                                <th>employee name</th>
                                                <th>title</th>
                                                <th>description</th>
                                                <th>weight</th>
                                                <th>type</th>
                                                <th>status</th>
                                                <th>location</th>
                                                <th>address</th>
                                                <th>delivery_date</th>
                                                <th>Edit status</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>track id</th>
                                                <th>deliver name </th>
                                                <th>employee name</th>
                                                <th>title</th>
                                                <th>description</th>
                                                <th>weight</th>
                                                <th>type</th>
                                                <th>status</th>
                                                <th>location</th>
                                                <th>address</th>
                                                <th>delivery_date</th>
                                                <th>Edit status</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <?php while ($fdata = $mydeliver->fetch(PDO::FETCH_ASSOC)) : ?>

                                                <?php

                                                $deliverIDD = $fdata['hash_id'];
                                                $quer = $db->query("SELECT * FROM `package` WHERE hash_id='$deliverIDD'");
                                                $fquer = $quer->fetch(PDO::FETCH_ASSOC);
                                                $to_id = $fquer['to_id'];
                                                $emp_id = $fquer['assign_id'];
                                                $getdata = $db->query("SELECT * FROM `user` WHERE id='$to_id'");
                                                $fgetdata = $getdata->fetch(PDO::FETCH_ASSOC);

                                                $getedata = $db->query("SELECT * FROM `user` WHERE id='$emp_id'");
                                                $fegetdata = $getedata->fetch(PDO::FETCH_ASSOC);
                                                ?>
                                                <tr>
                                                    <td> <?php echo "" . $fdata['hash_id'] . ""; ?></td>
                                                    <td> <?php echo "" . $fgetdata['name'] . ""; ?>
                                                    
                                                </td>
                                                <td> <?php echo "" . $fegetdata['name'] . ""; ?>
                                                    phone:<?php echo "" . $fegetdata['phone'] . ""; ?>
                                                </td>

                                                    <td> <?php echo "" . $fdata['title'] . ""; ?></td>
                                                    <td> <?php echo "" . $fdata['description'] . ""; ?></td>
                                                    <td> <?php echo "" . $fdata['weight'] . ""; ?></td>
                                                    <td> <?php echo "" . $fdata['type'] . ""; ?></td>


                                                    <td>
                                                        <?php
                                                        if ($fdata['status'] == "declined") {
                                                        ?>
                                                            <span class="badge badge-boxed badge-danger"> <?php echo "" . $fdata['status'] . ""; ?></span>
                                                        <?php
                                                        } else {
                                                        ?>
                                                            <span class="badge badge-boxed badge-info"> <?php echo "" . $fdata['status'] . ""; ?></span>
                                                        <?php
                                                        }
                                                        ?>

                                                    </td>

                                                    <td> <?php echo "" . $fdata['location'] . ""; ?></td>
                                                    <td> <?php echo "" . $fdata['address'] . ""; ?></td>
                                                    <td> <?php echo "" . $fdata['delivery_date'] . ""; ?></td>
                                                    <td>
                                                        <a href="update_courierpackage.php?id=<?php echo "" . $fdata['id'] . ""; ?>" data-toggle="tooltip" data-original-title="Create">
                                                            <i style="font-size: 35px;" class="fa fa-telegram text-success"></i>
                                                        </a>
                                                    </td>


                                                </tr>
                                            <?php endwhile; ?>
                                        </tbody>
                                    </table>
                                <?php
                                }
                                ?>

                                <!-- end corier dashboard -->

                            </div>

                            <div id="user_details"></div>
                            <div id="user_model_details"></div>

                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="./libs/jquery/dist/jquery.min.js"></script>
    <script src="./libs/nicescroll/jquery.nicescroll.min.js"></script>
    <script src="./libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="./libs/bootstrap/bootstrap.min.js"></script>
    <script src="./libs/PACE/pace.min.js"></script>
    <script src="./libs/chart.js/dist/Chart.min.js"></script>
    <script src="./libs/jquery-loading/dist/jquery.loading.min.js"></script>
    <script src="./libs/tables-datatables/dist/datatables.min.js"></script>
    <script src="./js/app.js"></script>
    <script src="./js/table-datatable-example.js"></script>
    <script>
        $(document).ready(function() {

            function make_chat_dialog_box(to_user_id, to_user_name) {
                var modal_content = '<div id="user_dialog_' + to_user_id + '" class="user_dialog" title="You have chat with ' + to_user_name + '">';
                modal_content += '<div style="height:400px; border:1px solid #ccc; overflow-y: scroll; margin-bottom:24px; padding:16px;" class="chat_history" data-touserid="' + to_user_id + '" id="chat_history_' + to_user_id + '">';
                modal_content += fetch_user_chat_history(to_user_id);
                modal_content += '</div>';
                modal_content += '<div class="form-group">';
                modal_content += '<textarea name="chat_message_' + to_user_id + '" id="chat_message_' + to_user_id + '" class="form-control"></textarea>';
                modal_content += '</div><div class="form-group" align="right">';
                modal_content += '<button type="button" name="send_chat" id="' + to_user_id + '" class="btn btn-info send_chat">Send</button></div></div>';
                $('#user_model_details').html(modal_content);
            }

            $(document).on('click', '.start_chat', function() {
                var to_user_id = $(this).data('touserid');
                var to_user_name = $(this).data('tousername');

                make_chat_dialog_box(to_user_id, to_user_name);
                $("#user_dialog_" + to_user_id).dialog({
                    autoOpen: false,
                    width: 400
                });
                $('#user_dialog_' + to_user_id).dialog('open');
            });

            $(document).on('click', '.send_chat', function() {
                var to_user_id = $(this).attr('id');
                var chat_message = $('#chat_message_' + to_user_id).val();
                $.ajax({
                    url: "./webservices/ajax_chat.php",
                    method: "POST",
                    data: {

                        to_user_id: to_user_id,
                        chat_message: chat_message
                    },
                    success: function(data) {
                        $('#chat_message_' + to_user_id).val('');
                        $('#chat_history_' + to_user_id).html(data);
                    }
                })
            });

            function fetch_user_chat_history(to_user_id) {
                $.ajax({
                    url: "./webservices/ajax_chathistory.php",
                    method: "POST",
                    data: {
                        to_user_id: to_user_id
                    },
                    success: function(data) {
                        $('#chat_history_' + to_user_id).html(data);
                    }
                })
            }

            function update_chat_history_data() {
                $('.chat_history').each(function() {
                    var to_user_id = $(this).data('touserid');
                    fetch_user_chat_history(to_user_id);
                });
            }
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#update_vehicleinfo").on('submit', (function(e) {
                e.preventDefault();
                $.ajax({
                    url: "./webservices/ajax_updatevehicle.php",
                    type: "POST",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data) {
                        var obj = $.parseJSON(data);
                        if (obj.success == "success") {
                            $("#success").show();
                            $("#success").html("vehicle details updated successfully");
                            $("#success").fadeOut(4000);
                            window.setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }
                        if (obj.success == "fail") {
                            $("#warning").show();
                            $("#warning").html("try After Sometime");
                            $("#warning").fadeOut(4000);
                            window.setTimeout(function() {
                                location.reload()
                            }, 3000);
                        }

                    }
                });
            }));
        });
    </script>

</body>

</html>